/** @jsx jsx */
/** @jsxRuntime classic */
/* eslint-disable jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { css, jsx } from '@emotion/react'
import type { PropsWithChildren } from 'react'
import { useCallback, useEffect } from 'react'
import ReactDOM from 'react-dom'

type Props = PropsWithChildren<{
  readonly show: boolean
  readonly title: string
  readonly onToggle: (show: boolean) => void
  readonly closeBackdrop?: boolean
  readonly closeButton?: boolean
}>

export const Modal = ({
  show,
  title,
  children,
  onToggle,
  closeBackdrop = true,
  closeButton = true
}: Props): JSX.Element | null => {
  const closeOnEscKeyDown = useCallback(
    (event: KeyboardEvent): void => {
      if (event.code === 'Escape') {
        onToggle(false)
      }
    },
    [onToggle]
  )

  useEffect(() => {
    document.addEventListener('keydown', closeOnEscKeyDown)
    return () => {
      document.removeEventListener('keydown', closeOnEscKeyDown)
    }
  }, [closeOnEscKeyDown])

  return ReactDOM.createPortal(
    <div
      onClick={(): void => {
        onToggle(!closeBackdrop)
      }}
      css={css`
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(0, 0, 0, 0.5);
        display: flex;
        justify-content: center;
        align-items: center;
        padding-bottom: 30%;
        opacity: ${show ? '1' : '0'};
        pointer-events: ${show ? 'visible' : 'none'};
        transition: all 0.3s ease-in-out;
      `}
    >
      <div
        onClick={(e): void => {
          e.stopPropagation()
        }}
        css={css`
          width: 420px;
          background-color: #fff;
          transform: translateY(${show ? '0' : '-200px'});
          transition: all 0.3s ease-in-out;
        `}
      >
        <div
          css={css`
            padding: 12px;
            text-align: center;
            position: relative;
          `}
        >
          {title}
          {closeButton && (
            <button
              type='button'
              css={css`
                border: none;
                cursor: pointer;
                position: absolute;
                right: 12px;
                top: 12px;
              `}
              onClick={(): void => {
                onToggle(false)
              }}
            >
              X
            </button>
          )}
        </div>
        {Boolean(children) && (
          <div
            css={css`
              padding: 12px;
            `}
          >
            {children}
          </div>
        )}
      </div>
    </div>,
    document.getElementById('root')!
  )
}
