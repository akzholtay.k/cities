/* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-explicit-any,
@typescript-eslint/explicit-function-return-type, @typescript-eslint/no-unsafe-argument,
 @typescript-eslint/restrict-template-expressions */

import { request } from 'graphql-request'
import type { RequestDocument } from 'graphql-request/dist/types'

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const apiRequest = async <T>(gqlRequest: RequestDocument, variables?: any): Promise<any> => {
	const url = process.env.REACT_APP_API_BASE_URL!
	return request<T>(
		url,
		gqlRequest,
		{ ...variables },
		{ Authorization: `Bearer ${localStorage.getItem('accessToken')}` }
	)
}
