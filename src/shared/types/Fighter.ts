export class Fighter {
  public color: string

  public targetID: string;

  public constructor(color: string, targetID: string) {
    this.color = color
    this.targetID = targetID;
  }

  // eslint-disable-next-line class-methods-use-this
  public draw(context: CanvasRenderingContext2D): void {
    context.beginPath()
    context.arc(20, 20, 30, 0, Math.PI * 2, false)
    context.fillStyle = this.color
    context.fill()
  }
}