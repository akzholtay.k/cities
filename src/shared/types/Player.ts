export class Player {
  public color: string

  public id: string

  public x: number

  public y: number

  public radius: number

  public constructor(c: string, id: string) {
    this.color = c
    this.id = id
    this.x = 50
    this.y = 50
    this.radius = 5
  }

  public draw(context?: CanvasRenderingContext2D | null): void {
    if (context) {
      console.log('Draw', this.radius);
      context.beginPath()
      context.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
      context.fillStyle = this.color
      context.fill()
    }
  }

  public clicked(x: number, y: number): void {
    console.log('click', this.radius, x, y);
    const dist = Math.hypot(x - this.x, y - this.y)
    if (dist - this.radius < 1) {
      this.radius /= 2
      console.log('target', this.radius)
    }
  }

  public scale(): void {
    if (this.radius < 40) {
      this.radius *= 2
    }
  }
}
