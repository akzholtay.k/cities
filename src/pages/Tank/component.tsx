/** @jsx jsx */
/** @jsxRuntime classic */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { css, jsx } from '@emotion/react'
import type { MouseEvent } from 'react'
import { useCallback, useEffect, useRef, useState } from 'react'
import { Player } from './types/Player'
import { Particle } from './types/Particle'
import { Enemy } from './types/Enemy'

export const Tank = (): JSX.Element => {
  const canvasRef = useRef<HTMLCanvasElement>(null)
  const [ctx, setCtx] = useState<CanvasRenderingContext2D>()
  const [player, setPlayer] = useState<Player>()
  const [particles, setParticles] = useState<Particle[]>([])
  const [enemies, setEnemies] = useState<Enemy[]>([])
  const requestRef = useRef<number>()

  const onStartGame = (): void => {
    const canvas = canvasRef.current
    const context = canvas?.getContext('2d')
    if (context && canvas) {
      setPlayer(new Player(canvas.width / 2, canvas.height, 'green'))
      setCtx(context)
    }
  }

  const onMouseEnter = (event: MouseEvent<HTMLCanvasElement>): void => {
    console.log('enter', event)
  }
  const onMouseLeave = (event: MouseEvent<HTMLCanvasElement>): void => {
    console.log('leave', event)
  }

  const onMouseMove = (event: MouseEvent<HTMLCanvasElement>): void => {
    const rect = canvasRef.current!.getBoundingClientRect()
    const canvas = canvasRef.current
    const x = Math.min(
      event.clientX - rect.x,
      canvas!.width - (player?.getWidth ?? 0)
    )
    if (player) {
      player.setX = x
    }
  }

  useEffect(() => {
    if (ctx) {
      player?.draw(ctx)
    }
  }, [ctx, player])

  useEffect(() => {
    const timer = setInterval(() => {
      const p = new Particle(player?.x ?? 0, player?.y ?? 0, 'yellow')
      setParticles(val => {
        const rest = val.filter(item => item.y > 0)
        return [...rest, p]
      })
    }, 400)
    return () => {
      clearInterval(timer)
    }
  }, [player?.x, player?.y])

  useEffect(() => {
    const timer2 = setInterval(() => {
      const rand = Math.random() * 25 // one of 25 pixels
      const x = rand * 20 // exact x
      const en = new Enemy(x, 20, 'red')
      setEnemies(val => {
        const filtered = val.filter(
          e =>
            !particles.find(p => {
              const dist = Math.hypot(p.x - e.x, p.y - e.y)
              return dist - Math.sqrt(800) < 1
            })
        )
        return [...filtered, en]
      })
    }, 1000)

    return () => {
      clearInterval(timer2)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const animate = useCallback((): void => {
    console.log('animate')
    requestRef.current = requestAnimationFrame(animate)
    if (ctx) {
      const canvas = canvasRef.current
      ctx.clearRect(0, 0, canvas!.width, canvas!.height)
      particles.forEach(p => {
        p.draw(ctx)
        p.move()
      })
      enemies.forEach(e => {
        e.draw(ctx)
        e.move()
      })

      player?.draw(ctx)
    }
  }, [ctx, enemies, particles, player])

  useEffect(() => {
    requestRef.current = requestAnimationFrame(animate)
    return (): void => {
      cancelAnimationFrame(requestRef.current ?? 0)
    }
  }, [animate])

  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
      `}
    >
      <div>
        <button type='button' onClick={onStartGame}>
          Start
        </button>
      </div>
      <canvas
        ref={canvasRef}
        width={500}
        height={700}
        css={css`
          border: 1px solid #1000ff;
        `}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        onMouseMove={onMouseMove}
      />
    </div>
  )
}
