export class Particle {
  public color: string

  public id: string

  public x: number

  public y: number

  public width = 20

  public height = 20

  public speed = 4

  public constructor(x: number, y: number, color: string) {
    this.color = color
    this.id = '13'
    this.x = x
    this.y = y - this.height
  }

  public draw(context: CanvasRenderingContext2D, x?: number, y?: number): void {
    context.beginPath()
    context.lineWidth = 1
    context.strokeStyle = this.color
    context.fillStyle = this.color
    context.rect(x ?? this.x, y ?? this.y, this.width, this.height)
    context.fill()
    context.stroke()
  }

  public move(): void {
    this.y -= this.speed
  }
}
