export class Player {
  public color: string

  public id: string

  public x: number

  public y: number

  public width = 20

  public height = 20

  public constructor(x: number, y: number, color: string) {
    this.color = color
    this.id = '12'
    this.x = x
    this.y = y - this.height
  }

  public get getWidth(): number {
    return this.width
  }

  public set setX(x: number) {
    this.x = x
  }

  public draw(context: CanvasRenderingContext2D, x?: number, y?: number): void {
    context.beginPath()
    context.lineWidth = 1
    context.strokeStyle = this.color
    context.fillStyle = this.color
    context.rect(x ?? this.x, y ?? this.y, this.width, this.height)
    context.fill()
    context.stroke()
  }
}
