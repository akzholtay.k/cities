/** @jsx jsx */
/** @jsxRuntime classic */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-extraneous-class */
import type { FC } from 'react'
import './styles.less'
import { useEffect, useState } from 'react'
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { css, jsx } from '@emotion/react'

import { Modal } from 'shared/components'
import { ColorArray, Game } from './types'

export const Colors: FC = () => {
  const [indexes, setIndexes] = useState<number[]>([])
  const [percent, setPercent] = useState<number>(100)
  const [game, setGame] = useState<Game>()
  const [correctAns, setCorrectAns] = useState<number>()
  const [showModal, setShowModal] = useState<boolean>(false)

  const onToggleModal = (show: boolean): void => {
    setShowModal(show)
  }
  const startGame = (addScore: boolean): void => {
    if (game) {
      setPercent(100)
      setCorrectAns(Math.floor(Math.random() * 3))
      setIndexes(game.next())
      if (addScore) {
        game.addScore()
      }
    }
  }
  const gameOver = (): void => {
    setShowModal(true)
  }
  const submitColor = (index: number): void => {
    if (index === correctAns) {
      startGame(true)
    } else {
      gameOver()
    }
  }

  useEffect(() => {
    setPercent(100)
    setGame(new Game())
  }, [])

  useEffect(() => {
    startGame(false)
  }, [game])

  useEffect(() => {
    const timer = setInterval(() => {
      setPercent(val => {
        const newVal = val - 10
        if (newVal > 0) {
          return val - 10
        }
        gameOver()
        return 0
      })
    }, 1000)

    return () => {
      clearInterval(timer)
    }
  }, [])

  return (
    <div>
      <div
        css={css`
          display: flex;
          flex-direction: column;
          padding: 24px;
          justify-content: center;
          align-items: center;
        `}
      >
        <h1>Score: {game?.score}</h1>

        <div
          css={css`
            position: relative;
            width: 120px;
            height: 120px;
          `}
        >
          <svg
            css={css`
              position: relative;
              width: 100%;
              height: 100%;
            `}
          >
            <circle
              cx='60'
              cy='60'
              r='40'
              css={css`
                width: 100%;
                height: 100%;
                fill: none;
                stroke-width: 4px;
                ${correctAns != null && {
                  stroke: ColorArray[indexes[correctAns]]?.color
                }};
                stroke-linecap: round;
                stroke-dasharray: 260px;
                stroke-dashoffset: ${260 - percent * 2.6}px;
              `}
            />
          </svg>
          <div
            css={css`
              width: 70px;
              height: 70px;
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
              border-radius: 50%;
              ${correctAns != null && {
                backgroundColor: ColorArray[indexes[correctAns]]?.color
              }};
            `}
          />
        </div>

        <div
          css={css`
            display: flex;
            justify-content: center;
          `}
        >
          {indexes.map((n, index) => (
            <button
              key={n}
              type='button'
              css={css`
                border: 1px solid #9b9b9b;
                border-radius: 4px;
                width: 180px;
                height: 64px;
                margin: 0 8px;
                background-color: #fff;
                font-weight: bold;
                cursor: pointer;
                ${correctAns != null && {
                  color: ColorArray[indexes[correctAns]]?.color
                }};
              `}
              onClick={(): void => {
                submitColor(index)
              }}
              tabIndex={index}
            >
              {ColorArray[n].name}
            </button>
          ))}
        </div>
      </div>
      <Modal
        show={showModal}
        title='Game over!'
        onToggle={onToggleModal}
        closeBackdrop={false}
        closeButton={false}
      >
        <div
          css={css`
            display: flex;
            justify-content: center;
            align-items: center;
          `}
        >
          <button
            type='button'
            onClick={(): void => {
              setGame(new Game())
              startGame(false)
              onToggleModal(false)
            }}
          >
            Start again
          </button>
        </div>
      </Modal>
    </div>
  )
}
