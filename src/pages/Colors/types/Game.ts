import { ColorArray } from './ColorArray'

const getRandomNumber = (max: number): number => Math.floor(Math.random() * max)

const getRandomArrayIndexes = (length: number, count: number): number[] => {
  const arr: number[] = []
  while (arr.length < count) {
    const n = getRandomNumber(length)
    if (!arr.includes(n)) {
      arr.push(n)
    }
  }
  return arr
}

export class Game {
  private scoreValue: number

  public constructor() {
    this.scoreValue = 0
  }

  public get score(): number {
    return this.scoreValue
  }

  public set score(s: number) {
    this.scoreValue = s
  }

  // eslint-disable-next-line class-methods-use-this
  public next(): number[] {
    const indexes = getRandomArrayIndexes(ColorArray.length, 3)
    return indexes
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public addScore(): void {
    this.score += 1
  }
}
