export enum ColorEnum {
  Red = 'red',
  Blue = 'blue',
  Yellow = 'yellow',
  Green = 'green',
  Black = 'black',
  Purple = 'purple'
}
