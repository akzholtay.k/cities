export type Color = {
  id: number
  name: string
  color: string
}
export const ColorArray: Color[] = [
  { id: 0, name: 'Red', color: '#f12020' },
  { id: 1, name: 'Blue', color: '#1b00ff' },
  { id: 2, name: 'Yellow', color: '#ffda18' },
  { id: 3, name: 'Orange', color: '#ff7d00' },
  { id: 4, name: 'Green', color: '#3dea00' },
  { id: 5, name: 'Black', color: '#000000' },
  { id: 6, name: 'Purple', color: '#9146ff' }
]
