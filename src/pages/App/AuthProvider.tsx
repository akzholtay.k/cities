/* eslint-disable @typescript-eslint/no-unused-vars */
import type { ReactNode } from 'react'
import React, { useMemo } from 'react'

interface User {
	email?: string
}
export interface AuthContextType {
	login: (token: string, callback?: VoidFunction) => void
	logout: (callback?: VoidFunction) => void
	setToken: (token: string) => void
	getToken: () => string
	setAccessToken: (token: string) => void
	getAccessToken: () => string
	setUser: (user: User) => void
	getUser: () => User
}

export const AuthContext = React.createContext<AuthContextType>({
	getUser(): User {
		return {}
	},
	setUser(_user: User): void {},
	getAccessToken(): string {
		return ''
	},
	getToken(): string {
		return ''
	},
	login(_token: string): void {},
	logout(): void {},
	setAccessToken(_token: string): void {},
	setToken(_token: string): void {}
})

export const AuthProvider = ({
	children
}: {
	children: ReactNode
}): JSX.Element => {
	// TODO: Add user and company info
	const setUser = (user: User): void => {
		localStorage.setItem('user', JSON.stringify(user))
	}
	const getUser = (): User => {
		const user = localStorage.getItem('user') ?? ''
		return JSON.parse(user) as User
	}
	const setToken = (token: string): void => {
		localStorage.setItem('TOKEN', token)
	}
	const getToken = (): string => {
		const token = localStorage.getItem('TOKEN')
		return token ?? ''
	}
	const setAccessToken = (token: string): void => {
		localStorage.setItem('accessToken', token)
	}
	const getAccessToken = (): string => {
		const token = localStorage.getItem('accessToken')
		return token ?? ''
	}
	const login = (token: string, callback?: VoidFunction): void => {
		localStorage.setItem('accessToken', token)
		callback?.()
	}
	const logout = (callback?: VoidFunction): void => {
		localStorage.setItem('accessToken', '')
		callback?.()
	}
	const value = useMemo(
		() => ({
			setAccessToken,
			getAccessToken,
			setToken,
			getToken,
			login,
			logout,
			setUser,
			getUser
		}),
		[]
	)

	return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}
