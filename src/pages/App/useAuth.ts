import React from 'react'
import type { AuthContextType } from "./AuthProvider";
import { AuthContext } from "./AuthProvider";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const useAuth = (): AuthContextType => React.useContext(AuthContext)

export default useAuth
