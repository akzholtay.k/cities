import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { routes } from 'pages/App/routes'
import { NotFound } from 'pages/NotFound'
import './App.less'
import type { ReactElement } from 'react'
import { AuthProvider } from './AuthProvider'
import RequireAuth from './RequireAuth'
import 'leaflet/dist/leaflet.css'
import { Field } from '../Field'

export const App = (): ReactElement => (
  <AuthProvider>
    <BrowserRouter>
      <Routes>
        <Route
          path='/profile'
          element={
            <RequireAuth>
              <Field />
            </RequireAuth>
          }
        />
        {routes.map(({ route, element }) => (
          <Route key={route} path={route} element={element} />
        ))}
        <Route path='*' element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  </AuthProvider>
)
