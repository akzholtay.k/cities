import type { ReactElement } from 'react'
import { Field } from '../Field'
import { Colors } from '../Colors'
import { Tank } from '../Tank'

export enum RoutesEnum {
  FIELD = '/field',
  EVENTS = '/events',
  COLORS = '/colors',
  TANK = '/tank'
}

export const routes: readonly {
  readonly route: RoutesEnum
  readonly element: ReactElement
}[] = [
  { route: RoutesEnum.FIELD, element: <Field /> },
  { route: RoutesEnum.COLORS, element: <Colors /> },
  { route: RoutesEnum.EVENTS, element: <Field /> },
  { route: RoutesEnum.TANK, element: <Tank /> }
]
