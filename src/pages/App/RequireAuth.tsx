import { useLocation, Navigate } from 'react-router-dom'
import useAuth from './useAuth'

const RequireAuth = ({ children }: { children: JSX.Element }): JSX.Element => {
	const auth = useAuth()
	const location = useLocation()
	if (!auth.getAccessToken() || !auth.getToken()) {
		return <Navigate to='/' state={{ from: location }} replace />
	}
	return children
}

export default RequireAuth
