import type {
  CanvasHTMLAttributes,
  ReactElement,
  RefAttributes,
  LegacyRef
} from 'react'

// TODO: remove onClick
type Props = CanvasHTMLAttributes<HTMLCanvasElement> &
  RefAttributes<HTMLCanvasElement> & {
    canvasRef: LegacyRef<HTMLCanvasElement> | null
  }

export const Canvas = ({ canvasRef, ...rest }: Props): ReactElement => (
  <canvas ref={canvasRef} {...rest} />
)
