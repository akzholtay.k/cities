/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-extraneous-class */
import type { FC } from 'react'
import './styles.less'
import { useEffect, useRef, useState } from 'react'
import { Canvas } from './libs/Canvas'
import { Player } from '../../shared/types'

export const Field: FC = () => {
  const [clickedX, setClickedX] = useState<number>()
  const [clickedY, setClickedY] = useState<number>()
  const canvasRef = useRef<HTMLCanvasElement>(null)
  const [ctx, setCtx] = useState<CanvasRenderingContext2D>()
  const [player, setPlayer] = useState<Player>()
  // const requestRef = useRef<number>()

  const onClickCanvas = (event: React.MouseEvent<HTMLCanvasElement>): void => {
    const rect = canvasRef.current!.getBoundingClientRect()
    const x = event.clientX - rect.x
    const y = event.clientY - rect.y
    setClickedX(x)
    setClickedY(y)
  }

  const onStartGame = (): void => {
    const canvas = canvasRef.current
    const context = canvas?.getContext('2d')
    if (context) {
      setCtx(context)
    }
  }

  useEffect(() => {
    if (Boolean(clickedX) && Boolean(clickedY)) {
      player?.clicked(clickedX!, clickedY!)
      ctx?.clearRect(0, 0, 1000, 1000)
      player?.draw(ctx)
    }
  }, [clickedX, clickedY])

  useEffect(() => {
    const timer = ctx
      ? setInterval(() => {
          player?.scale()
          ctx.clearRect(0, 0, 1000, 1000)
          player?.draw(ctx)
        }, 1000)
      : null
    return () => {
      if (timer) {
        clearInterval(timer)
      }
    }
  }, [ctx])

  useEffect(() => {
    if (ctx) {
      player?.draw(ctx)
    }
  }, [ctx])

  useEffect(() => {
    console.log('New PLayer');
    setPlayer(new Player('green', '12'))
  }, [])
  // const animate = (): void => {
  //   requestRef.current = requestAnimationFrame(animate)
  //   player.draw(ctx)
  // }
  // useEffect(() => {
  //   requestRef.current = requestAnimationFrame(animate)
  //   return (): void => {
  //     cancelAnimationFrame(requestRef.current ?? 0)
  //   }
  // }, [])

  return (
    <div>
      <button type='button' onClick={onStartGame}>
        Start
      </button>
      <Canvas
        width={window.innerWidth}
        height={window.innerHeight}
        onClick={onClickCanvas}
        canvasRef={canvasRef}
      />
    </div>
  )
}
